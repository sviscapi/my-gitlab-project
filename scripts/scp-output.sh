#!/bin/bash

#***********************************
# Samuel VISCAPI - LAB - 2013/10/23
#
# email@host.com
#***********************************

# Fetches output directory of a given task from xxx, on the fly

# Usage:

# scp-output.sh login_xx task_name task_type output_directory

# Ex: scp-output.sh john AG-GR-xx-xx-xx xx /home/john/myoutput

if [ ! $# == 4 ]
then
echo "Usage: scp-output.sh login_xx task_name task_type output_dir"
echo "Ex: scp-output.sh john AG-GR-xx-xx-xx xxx /home/john/myoutput"
fi

if [ ! -d $4 ]
then
echo "Output directory doesn't exist yet, creating it"
mkdir -p $4
fi

if [ ! "$3" == "xxx" -a ! "$3" == "xxx" ]
then
echo "Task type can be xxx or xxx"
fi

if [ "$3" == "xxx" ]
then 
scp -r $1@host:/path/to/task/$2/output $4
fi

if [ "$3" == "xxx" ]
then
scp -r $1@host:/path/to/task/$2/output $4
fi
