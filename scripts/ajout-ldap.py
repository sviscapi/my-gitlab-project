#! /usr/bin/python
# -*- coding: utf-8 -*-

# On importe les bibliothèques utiles

import os
import urllib
import subprocess

# On renseigne les données sur l'étudiant

prenom = str.capitalize(raw_input('Entrez prénom (sans accents SVP): '))
print ('Prenom: ', prenom)
nom = str.capitalize(raw_input('Entrez nom (sans accents SVP): '))
print ('Nom: ', nom)
prenom_lower = str.lower(prenom)
nom_lower = str.lower(nom)
#print ('Prenom en minuscules: ', prenom_lower)
#print ('Nom en minuscules: ', nom_lower)
my_uid = prenom_lower[0] + nom_lower
print ('Identifiant est: ', my_uid)
#my_uid_number = subprocess.check_output("ldapsearch -x -LLL -H ldap://host:389 -b dc=stage8,dc=lab uid=*  uidNumber  | grep uidNumber | sed s/'uidNumber: '/''/ | sort -gr | head -n1", shell=True)
#print ('UID Number est: ', my_uid_number)
my_outputfile = 'add-content-'+ prenom + nom + '.ldif'
print ('Fichier de sortie est: ', my_outputfile)

# On écrit les données dans un fichier LDIF

#with open('/usr/local/ldap-stage8/'+my_outputfile, 'w') as f:
with open(my_outputfile, 'w') as f:

	f.write('dn: uid=my_uid,ou=People,dc=stage8,dc=lab\n')
	f.write('objectClass: inetOrgPerson\n')
	f.write('objectClass: posixAccount\n')
	f.write('objectClass: shadowAccount\n')
	f.write('uid: '+my_uid+'\n')
	f.write('sn: '+prenom+'\n')
	f.write('givenName: '+nom+'\n')
	f.write('cn: '+prenom+' '+nom+'\n') 
	f.write('displayName: '+my_uid+'\n')
	#f.write('uidNumber: '+my_uid_number+'\n')
	f.write('gidNumber: 500\n')
	f.write('userPassword: lab-'+my_uid+'\n')
	f.write('loginShell: /bin/bash\n')
	f.write('homeDirectory: /home/'+my_uid+'\n')

# On lance la commande ldapadd avec le fichier LDIF

#subprocess.check_call("ldapadd -x -D cn=admin,dc=stage8,dc=lab -W -f" my_outputfile, shell=True)

# Creation du répertoire avec les droits qui vont bien

os.mkdir('/tmp/'+my_uid)
#os.chown('/tmp/'+my_uid, my_uid, -1)
os.chmod('/tmp/'+my_uid, 0700)

# On met la charte informatique dans le repertoire

os.chdir('/tmp/'+my_uid)
urllib.urlretrieve("url_to_file.pdf", filename='/tmp/'+my_uid+'/file.pdf')
